from django.db import models

class Contact(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50, blank=False, null=False)
    email = models.EmailField(max_length=50, blank=False)
    phone = models.CharField(max_length=17, blank=False, null=False)
    image = models.ImageField(blank=True, null=True)