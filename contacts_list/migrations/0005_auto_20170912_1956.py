# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-09-12 16:56
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contacts_list', '0004_auto_20170912_1947'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contact',
            name='phone',
            field=models.CharField(max_length=15),
        ),
    ]
