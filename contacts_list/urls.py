from django.conf.urls import url

from contacts.settings import MEDIA_ROOT, DEBUG, MEDIA_URL
from .views import contacts_list, contact_edit, contact_delete, contact_create

urlpatterns = [
    url(r'^$', contacts_list, name='contacts_list'),
    url(r'^contacts/create/$', contact_create, name='create'),
    url(r'^contacts/(?P<uid>\d+)/edit/$', contact_edit, name='edit'),
    url(r'^contacts/(?P<uid>\d+)/delete/$', contact_delete, name='delete'),
]

if DEBUG:
    from django.conf.urls.static import static
    urlpatterns += static(
        MEDIA_URL,
        document_root=MEDIA_ROOT)