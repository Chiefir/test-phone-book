

def phone_validator(phone):
    '''Custom phone number validator for use in forms and views'''
    if phone.replace('-', '').isdigit() and len(phone) == 13:
        phone = '+38-' + phone
        return phone
    else:
        return None


