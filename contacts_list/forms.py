from django import forms

from .models import Contact
from .tools import phone_validator

class ContactForm(forms.ModelForm):
    '''Create Contact Form'''
    class Meta:
        model = Contact
        fields ={'name', 'email', 'phone', 'image'}
        widgets = {
            'phone': forms.TextInput(attrs={'placeholder': 'format: 099-999-99-99'})
        }

    def clean_phone(self):
        '''Validator for phone number'''
        phone = self.cleaned_data['phone']
        phone_validated = phone_validator(phone)
        return phone_validated

