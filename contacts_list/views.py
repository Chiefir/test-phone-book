from django.shortcuts import render, redirect
from django.core.validators import validate_email
from django.core.exceptions import ValidationError

from .models import Contact
from .forms import ContactForm
from .tools import phone_validator

def contacts_list(request):
    '''View for Contact List rendering'''
    if len(Contact.objects.all()) == 0:         #Just for test, if database is empty. Need to create Contact first!
        contacts = [                            #Links won't work untill base is empty.
            {'id': 1,
             'name': 'Vanessa May',
             'email': 'aa@aa.com',
             'phone': '+38-050-050-50-50'},
            {'id': 2,
             'name': 'Donald Trump',
             'email': 'aa@aa.ua',
             'phone': '+38-050-080-80-80'}
        ]
    else:
        contacts = Contact.objects.order_by('id')

    return render (request, 'contact-list.html', {"contacts": contacts})

def contact_create(request):
    '''Contact Create view'''
    if request.method == 'POST':
        form = ContactForm(request.POST, request.FILES)
        if form.is_valid():
            new_contact = form.save()
            return redirect(contacts_list)
        else:
            return render (request, 'contact-create.html', {'form': form})
    else:
        form = ContactForm()
        return render(request, 'contact-create.html', {'form': form})

def contact_edit(request, uid):
    '''Contact Edit view'''
    if request.method == 'POST':
        db_user = Contact.objects.get(id=uid)
        db_user.name = request.POST.get('name')
        db_user.email = request.POST.get('email')
        db_user.phone = request.POST.get('phone')
        db_user.image = request.FILES.get('image')

        '''Email and Phone number validation'''
        errors = []
        try:
            validate_email(request.POST.get('email'))
        except ValidationError:
            errors.append('Wrong email format!')

        if not phone_validator(request.POST.get('phone')):
            errors.append('Wrong phone number format! Try: 099-999-99-99')

        if errors:
            return render(request, 'contact-edit.html', {'user': db_user, 'errors': errors})
        else:
            db_user.save()
            return redirect(contacts_list)

    else:
        user = Contact.objects.get(id = uid)
        return render(request, 'contact-edit.html', {'user': user})


def contact_delete(request, uid):
    '''View for delete contact'''
    if request.method == 'POST':
        user_del = Contact.objects.filter(id = uid).delete()
        return redirect(contacts_list)
    else:
        user = Contact.objects.get(id = uid)
        return render(request, 'contact-delete.html', {"user": user})
